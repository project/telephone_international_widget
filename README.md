CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

A field widget for entering, formatting, and validating international telephone
numbers.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/telephone_international_widget

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/telephone_international_widget


REQUIREMENTS
------------

This module requires no module outside of Drupal core.


INSTALLATION
------------

 * Install the Telephone International Widget module as you would normally
   install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Content types > Manage fields
    3. Add field with the type of Telephone number give it a Label and Save.
    4. Go to Manage form display find your Telephone number filed and change its
       Widget to International telephone.


MAINTAINERS
-----------

 * James Candan (jcandan) - https://www.drupal.org/u/jcandan
